
# The next line updates PATH for the Google Cloud SDK.
if [ -f '/Users/joel/Downloads/google-cloud-sdk/path.bash.inc' ]; 
  then source '/Users/joel/Downloads/google-cloud-sdk/path.bash.inc'; 
fi

# The next line enables shell command completion for gcloud.
if [ -f '/Users/joel/Downloads/google-cloud-sdk/completion.bash.inc' ]; 
    then source '/Users/joel/Downloads/google-cloud-sdk/completion.bash.inc'; 
fi

. ~/bin/git-completion.sh

parse_git_branch() {
  git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export PS1="\u@\h \[\033[32m\]\W\[\033[33m\]\$(parse_git_branch)\[\033[00m\]$ "
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export PATH=$PATH:~/bin:$GOPATH:$GOBIN:~/git/scripts:~/tmp/scripts:$HOME/.cargo/bin
export PATH=$PATH:"/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
export CORE=$GOPATH/src/gitlab.com/redeam/core
export CORE_DOCKER_IP=localhost

run_e2e() {
    cd $CORE/e2e-runners/channel-management && godotenv -f ./$1/redeamsa-all-on-$2.env go run channel-mgmt-e2e.go
}

gen_from_swagger() {
    cd $CORE/api/surfaces/$1/swagger/output && rm -rf /tmp/csharp && mkdir -p /tmp/csharp && swagger-codegen generate -i ./$2.$3.swagger.json -l csharp -o /tmp/csharp
}

alias c="cd $CORE && git log | head -n25 && git status"
alias tginkgo="LOG_LEVEL=error DEV_MODE=true ginkgo -r"

alias bc="tginkgo $CORE/api/surfaces/booking-api $CORE/api/surfaces/channel-api"
alias bc-v1.2="tginkgo $CORE/api/surfaces/booking-api/test-v1.2 $CORE/api/surfaces/channel-api/test-v1.2"
alias bs-prepush="build-all && bs-test-all && gen-all"
alias bs-test-all="bs && internal && ota-test && backplane"
alias bs="build-e2e && tginkgo -r $CORE/api $CORE/apitools"
alias backplane="tginkgo $CORE/backplane"
alias build-e2e="cd $CORE/e2e-runners/channel-management && go build channel-mgmt-e2e.go"
alias build-avail-cacher="cd $CORE/scripts/availabilities/cacher && go build cache-availabilities.go"
alias build-avail-checker="cd $CORE/scripts/availabilities/checker && go build check-for-rate.go"
alias build-migrate-partner="cd $CORE/scripts/migrate-partner && go build migrate.go"
alias build-validate-partner="cd $CORE/scripts/validate-partner-api && go build validate.go"
alias build-all="build-avail-cacher && build-avail-checker && build-migrate-partner && build-validate-partner"
alias internal="tginkgo $CORE/internal $CORE/services/res-adapter $CORE/services/bookingprocessor $CORE/services/domain-rate $CORE/services/domain-product $CORE/services/supplier"
alias ota-test="cd $CORE && tginkgo ./pkg/ota_shim ./services/expedia ./services/gyg"

alias e2e="run_e2e env/combined develop"
alias e2e-v1.2="run_e2e env-v1.2 develop"
alias e2e-sandbox="run_e2e env/combined sandbox"
alias e2e-sandbox-v1.2="run_e2e env-v1.2 sandbox"

alias psql-local="docker exec -it $(docker ps --filter 'name=postgres' --format '{{.ID}}') psql -U actourex -d actourex"

alias gen-all="gen-swagger && gen-backplane-swagger && code-gen"
alias gen-swagger="cd $CORE/api/surfaces && ./swagger-generator.pl --verbose"
alias gen-backplane-swagger="cd $CORE/backplane/swagger && ./generate-all.sh"
alias code-gen="code-gen-booking && code-gen-channel && code-gen-booking-v1.2 && code-gen-channel-v1.2"
alias code-gen-booking="gen_from_swagger booking-api sandbox v1.1"
alias code-gen-channel="gen_from_swagger channel-api sandbox v1"
alias code-gen-booking-v1.2="gen_from_swagger booking-api sandbox v1.2"
alias code-gen-channel-v1.2="gen_from_swagger channel-api sandbox v1.2"

ulimit -n 10000